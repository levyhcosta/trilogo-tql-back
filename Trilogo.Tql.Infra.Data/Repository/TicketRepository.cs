﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Text;
using Trilogo.Tql.Domain.Interfaces;
using Trilogo.Tql.Domain.Models;
using Trilogo.Tql.Infra.Data.Configuration;

namespace Trilogo.Tql.Infra.Data.Repository
{
    public class TicketRepository : Connection, ITicketRepository
    {
        public IEnumerable<Ticket> Find(string tql, IDictionary<string, object> parameters)
        {
            var results = ConnectionSql.Query<Ticket>("select * from Ticket " + tql, parameters);

            return results;
        }
    }
}
