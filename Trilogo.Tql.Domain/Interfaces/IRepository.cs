﻿using System.Collections.Generic;

namespace Trilogo.Tql.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Find(string tql, IDictionary<string, object> parameters);
    }
}
