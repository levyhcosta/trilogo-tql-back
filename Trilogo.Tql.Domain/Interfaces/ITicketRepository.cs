﻿using Trilogo.Tql.Domain.Models;

namespace Trilogo.Tql.Domain.Interfaces
{
    public interface ITicketRepository : IRepository<Ticket>
    {
    }
}
