﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trilogo.Tql.Domain.Models
{
    public class Ticket
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
