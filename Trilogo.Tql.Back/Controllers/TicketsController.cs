﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Trilogo.Tql.Application.DTO;
using Trilogo.Tql.Application.Interfaces;

namespace Trilogo.Tql.Back.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketsController : ControllerBase
    {
        ITicketService _ticketService;
        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpGet]
        public IEnumerable<TicketDTO> Get(string tql)
        {
            return _ticketService.Find(tql);
        }
    }
}