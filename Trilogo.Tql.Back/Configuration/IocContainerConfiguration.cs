﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trilogo.Tql.Application.Interfaces;
using Trilogo.Tql.Application.Services;
using Trilogo.Tql.Domain.Interfaces;
using Trilogo.Tql.Infra.Data.Repository;

namespace Trilogo.Tql.Back.Configuration
{
    public class IocContainerConfiguration
    {
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ITicketService, TicketService>();
            services.AddTransient<ITicketRepository, TicketRepository>();
        }
    }
}
