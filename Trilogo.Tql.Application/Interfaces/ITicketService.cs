﻿using System;
using System.Collections.Generic;
using System.Text;
using Trilogo.Tql.Application.DTO;

namespace Trilogo.Tql.Application.Interfaces
{
    public interface ITicketService
    {
        IEnumerable<TicketDTO> Find(string tql);
    }
}
