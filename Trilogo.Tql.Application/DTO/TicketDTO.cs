﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trilogo.Tql.Application.DTO
{
    public class TicketDTO
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
