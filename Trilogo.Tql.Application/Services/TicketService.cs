﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Trilogo.Tql.Application.DTO;
using Trilogo.Tql.Application.Interfaces;
using Trilogo.Tql.Application.Utils;
using Trilogo.Tql.Domain.Interfaces;
using Trilogo.Tql.Domain.Models;

namespace Trilogo.Tql.Application.Services
{
    public class TicketService : ITicketService
    {
        ITicketRepository _ticketRepository;
        private readonly IMapper mapper;
        public TicketService(ITicketRepository ticketRepository, IMapper mapper)
        {
            _ticketRepository = ticketRepository;
            this.mapper = mapper;
        }

        public IEnumerable<TicketDTO> Find(string tql)
        {
            var tqlHelper = new TqlHelper<Ticket>(tql);

            var results = _ticketRepository.Find(tqlHelper.GetQuery(), tqlHelper.GetParameters());

            return mapper.Map<IEnumerable<Ticket>, IEnumerable<TicketDTO>>(results);
        }
    }
}
