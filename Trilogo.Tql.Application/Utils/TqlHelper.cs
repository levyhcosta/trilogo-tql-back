﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Trilogo.Tql.Application.Utils
{
    public class TqlHelper<T> where T : class
    {
        /// <summary>
        /// Key = Nome do parametro, Value = Valor 
        /// </summary>
        private IDictionary<string, object> _parameters;
        /// <summary>
        /// Key = Ordem, Value = query
        /// </summary>
        private IDictionary<int, string> _queryDic;
        private readonly string[] ArrayOperators = new string[] { "=", "!=", "IN", "in", "~", ">", ">=", "<", "<=" };

        public TqlHelper(string tql)
        {
            _parameters = new Dictionary<string, object>();
            _queryDic = new Dictionary<int, string>();
            this.GenerateFilds(tql);
        }

        /// <summary>
        /// Retorna a query string de acordo com a ordem
        /// </summary>
        /// <returns></returns>
        public string GetQuery()
        {
            StringBuilder queryFull = new StringBuilder();

            _queryDic.OrderBy(x => x.Key).ToList().ForEach(x => { queryFull.Append(x.Value); });

            return string.IsNullOrEmpty(queryFull.ToString()) ? "" : "Where " + queryFull.ToString();
        }

        public IDictionary<string, object> GetParameters()
        {
            return this._parameters;
        }

        private void GenerateFilds(string tql)
        {
            int countParameters = 0;
            tql = tql.Replace(@"\", "").Replace(@"""", "");

            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                // Verifica se o TQL possui a propriedade da classe
                if (tql.IndexOf(property.Name, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    // Recupera o indice da propriedade
                    int indexPropName = tql.IndexOf(property.Name, StringComparison.OrdinalIgnoreCase);
                    // Recupera o campo e o operador
                    string fildAndOperator = tql.Substring(indexPropName, (property.Name.Length + 3));
                    // Se o tql estiver correto, aqui ficará o operador
                    string strOperator = fildAndOperator.Substring(property.Name.Length).Replace(" ", "");


                    string dicKey = "";
                    countParameters++;

                    if (ArrayOperators.Contains(strOperator.ToLower()))
                    {
                        // Contains 
                        if (strOperator == "~")
                        {
                            dicKey = "Like Concat('%', @param" + countParameters + ", '%')";
                        }
                        else
                        {
                            dicKey = strOperator + " @param" + countParameters;
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Não foi possível interpretar o TQL, verifique se ele está de acordo com a norma");
                    }

                    // Indice da propriedade + o tamanho do nome + o tamanho do operador + 2 (Para os espaços)
                    int indexSub = (indexPropName + property.Name.Length + strOperator.Length + 2);
                    // Passa o tql sem o fild e o operador atual, ex: Tql='id = 1' fica Tql='1'
                    var value = GetValue(tql.Substring(indexSub), strOperator, property.Name + " " + dicKey, indexSub);

                    _parameters.Add("param" + countParameters, value);
                }
            }
        }

        private object GetValue(string tql, string strOperator, string query, int index)
        {
            // Verificar se possui alguma palavra chave
            int indexOr = tql.IndexOf("OR");
            int indexAnd = tql.IndexOf("AND");
            // Caso tenha duas palavras chaves, o menor index serve para verificar qual é a mais próxima
            int menorIndex = tql.Length;

            // _queryDic a key é o index do valor pois serve para ordenar a consulta de acordo com TQL, pois 
            // o algoritimo verifica na ordem das propriedades da classe e não na ordem do TQL, ex se fosse
            // TQL = 'author = "nomeAuthor" AND id = 3', o algoritimo iria gerar a query dessa forma errada:
            // 'id = @param1 author = @param2 AND'
            if (indexOr != -1 && indexAnd != -1)
            {
                if (indexOr < indexAnd)
                {
                    _queryDic.Add(index, query + " OR ");
                    menorIndex = indexOr;
                }
                else
                {
                    _queryDic.Add(index, query + " AND ");
                    menorIndex = indexAnd;
                }
            }
            else if (indexOr != -1)
            {
                _queryDic.Add(index, query + " OR ");
                menorIndex = indexOr;
            }
            else if (indexAnd != -1)
            {
                _queryDic.Add(index, query + " AND ");
                menorIndex = indexAnd;
            }
            else
            {
                _queryDic.Add(index, query);
            }

            string valor = tql.Substring(0, menorIndex).Trim();

            // IN
            if (strOperator == "in")
            {
                // Ex: 2,3,4
                string strNumbersIn = Regex.Replace(tql.Substring(0, menorIndex).ToString(), "[() ]+", "");
                // Separa os numeros
                int[] arrayNumbers = strNumbersIn.Split(',').Select(int.Parse).ToArray();
                return arrayNumbers;
            }
            else
            {
                int intAux = 0;

                if (int.TryParse(valor, out intAux))
                {
                    return intAux;
                }

                return valor;
            }
        }
    }
}
